package br.com.inter.digitoUnico.services;

import br.com.inter.digitoUnico.entities.DigitoUnico;
import br.com.inter.digitoUnico.repositories.DigitoUnicoRepository;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class DigitoUnicoServiceTest {

    @MockBean
    private DigitoUnicoRepository digitoUnicoRepository;

    @Autowired
    private DigitoUnicoService digitoUnicoService;

    @Before
    public void setUp(){
        BDDMockito.given(this.digitoUnicoRepository.save(Mockito.any(DigitoUnico.class))).willReturn(new DigitoUnico());
    }

    @Test
    public void testPersistirDigitoUnico(){
        DigitoUnico digitoUnico = digitoUnicoService.salvar(new DigitoUnico());
        TestCase.assertNotNull("Verifica se foi retornado o digito salvo.", digitoUnico);
    }

    @Test
    public void testCalculoDigitoUnico(){
        String resultadoEsperado = "8";
        DigitoUnico digitoUnico = digitoUnicoService.calcular(9875, 4);

        TestCase.assertEquals("Verifica se o calculo do dígito unico foi feito corretamente.", resultadoEsperado, digitoUnico.getResultado());
    }
}
