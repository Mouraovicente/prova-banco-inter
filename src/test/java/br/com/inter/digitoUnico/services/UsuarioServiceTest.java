package br.com.inter.digitoUnico.services;

import br.com.inter.digitoUnico.entities.Usuario;
import br.com.inter.digitoUnico.repositories.UsuarioRepository;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class UsuarioServiceTest {

    @MockBean
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioService usuarioService;

    @Before
    public void setUp(){
        BDDMockito.given(this.usuarioRepository.save(Mockito.any(Usuario.class))).willReturn(new Usuario());
        BDDMockito.given(this.usuarioRepository.findOne(Mockito.anyLong())).willReturn(new Usuario());
        Mockito.doNothing().when(this.usuarioRepository).delete(Mockito.anyLong());
    }

    @Test
    public void testSalvarUsuario(){
        Optional<Usuario> usuario = usuarioService.salvar(new Usuario());
        TestCase.assertNotNull("Verifica se foi retornado o usuario salvo.", usuario.isPresent());
    }

    @Test
    public void testBuscarUsuarioPorId(){
        Optional<Usuario> usuario = usuarioService.buscarPorId(1L);
        TestCase.assertTrue("Verificar se busca por id está retornando um usuário.", usuario.isPresent());
    }

    @Test
    public void testDeletarUsuario(){
        Optional<Usuario> usuario = usuarioService.deletar(1L);
        TestCase.assertTrue("Verificar o usuário deletado foi retornado.", usuario.isPresent());
    }

}
