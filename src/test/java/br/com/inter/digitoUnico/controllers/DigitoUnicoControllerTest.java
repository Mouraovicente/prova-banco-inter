package br.com.inter.digitoUnico.controllers;

import br.com.inter.digitoUnico.entities.DigitoUnico;
import br.com.inter.digitoUnico.entities.Usuario;
import br.com.inter.digitoUnico.services.DigitoUnicoService;
import br.com.inter.digitoUnico.services.UsuarioService;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class DigitoUnicoControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private DigitoUnicoService digitoUnicoService;

    @MockBean
    private UsuarioService usuarioService;

    private static final String CALCULAR_DIGITO_UNICO = "/api/digito-unico";

    @Test
    public void testCalcularDigitoUnico() throws Exception {
        BDDMockito.given(this.digitoUnicoService.calcular(Mockito.anyInt(), Mockito.anyInt())).willReturn(new DigitoUnico());
        BDDMockito.given(this.usuarioService.salvar(Mockito.any(Usuario.class))).willReturn(java.util.Optional.of(new Usuario()));
        BDDMockito.given(this.usuarioService.buscarPorId(Mockito.anyLong())).willReturn(java.util.Optional.of(new Usuario()));

        JSONObject json = new JSONObject();
        json.put("numero", 911);
        json.put("multiplicador", 2);
        json.put("idUsuario", 1);

        mvc.perform(MockMvcRequestBuilders.post(CALCULAR_DIGITO_UNICO)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.toString()))
                .andExpect(status().isOk());
    }

}
