package br.com.inter.digitoUnico.services.impl;

import br.com.inter.digitoUnico.entities.DigitoUnico;
import br.com.inter.digitoUnico.repositories.DigitoUnicoRepository;
import br.com.inter.digitoUnico.services.DigitoUnicoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@CacheConfig(cacheNames = {"digitos"})
public class DigitoUnicoServiceImpl implements DigitoUnicoService {

    private static final Logger log = LoggerFactory.getLogger(DigitoUnicoServiceImpl.class);

    @Autowired
    private DigitoUnicoRepository digitoUnicoRepository;

    /*
     * Método responsável por persistir o dígito único no banco.
     *
     * @param digitoUnico
     * @return DigitoUnico
     */
    @Override
    public DigitoUnico salvar(DigitoUnico digitoUnico) {
        log.info("Persisitindo digitoUnico {}", digitoUnico);
        return digitoUnicoRepository.save(digitoUnico);
    }

    /*
     * Método responsável por calcular o dígito único.
     *
     * @param numero
     * @param multiplo
     * @return DigitoUnico
     */
    @Cacheable
    @Override
    public DigitoUnico calcular(Integer numero, Integer multiplo) {
        log.info("Calculando o dígito único para o número {} e o multiplo {}", numero, multiplo);
        String numeroParaCalculo = numero.toString();

        if(!StringUtils.isEmpty(multiplo)){
            numeroParaCalculo = aplicarMultiplicador(numeroParaCalculo, multiplo);
        }

        return new DigitoUnico(numero, multiplo, calcularDigitoUnico(numeroParaCalculo));
    }

    /*
     * Método responsável por aplicar o multiplicados ao dígito único.
     *
     * @param numero
     * @param multiplicador
     * @return String
     */
    private String aplicarMultiplicador(String numero, Integer multiplicador){
        StringBuilder numeroMultiplicado = new StringBuilder();

        for(int i = 0; i < multiplicador; i++){
            numeroMultiplicado.append(numero);
        }
        return numeroMultiplicado.toString();
    }

    /*
     * Método responsável por aplicar as regras do cálculo do dígito único.
     *
     * @param numero
     * @return String
     */
    private String calcularDigitoUnico(String numero){
        Integer somaNumeros = 0;

        if(numero.length() == 1){
            return numero;
        }

        for (char num: numero.toCharArray()) {
            somaNumeros += Integer.parseInt(Character.toString(num));
        }

        if(somaNumeros.toString().length() > 1){
            return calcularDigitoUnico(somaNumeros.toString());
        }

        return somaNumeros.toString();
    }
}
