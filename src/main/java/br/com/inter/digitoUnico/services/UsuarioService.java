package br.com.inter.digitoUnico.services;

import br.com.inter.digitoUnico.entities.Usuario;

import java.util.Optional;

public interface UsuarioService {

    /**
     * Retorna um usuário dado um id.
     *
     * @param id
     * @return Optional<Usuario>
     */
    Optional<Usuario> buscarPorId(Long id);

    /**
     * Cadastra um usuário no banco de dados.
     *
     * @param usuario
     * @return Usuario
     */
    Optional<Usuario> salvar(Usuario usuario);

    /**
     * Deletar um usuário no banco de dados.
     *
     * @param usuario
     * @return void
     */
    Optional<Usuario> deletar(Long id);

}
