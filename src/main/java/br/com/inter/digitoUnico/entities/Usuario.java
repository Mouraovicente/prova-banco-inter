package br.com.inter.digitoUnico.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name="usuario")
@Getter @Setter @NoArgsConstructor
public class Usuario implements Serializable{

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column
    private String nome;

    @Column
    private String email;

    @OneToMany(mappedBy = "usuario", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<DigitoUnico> digitoUnicoList;

    public Usuario(Long id) {
        this.id = id;
    }

	public Usuario() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<DigitoUnico> getDigitoUnicoList() {
		return digitoUnicoList;
	}

	public void setDigitoUnicoList(List<DigitoUnico> digitoUnicoList) {
		this.digitoUnicoList = digitoUnicoList;
	}
}
