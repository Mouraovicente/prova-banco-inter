package br.com.inter.digitoUnico.utils;

import br.com.inter.digitoUnico.dtos.DigitoUnicoDto;
import br.com.inter.digitoUnico.dtos.UsuarioDto;
import br.com.inter.digitoUnico.entities.DigitoUnico;
import br.com.inter.digitoUnico.entities.Usuario;
import br.com.inter.digitoUnico.dtos.*;

import java.util.ArrayList;
import java.util.List;

public class ConvesaoEntidadesUtil {

    /**
     * Converter um usuário para dto
     *
     * @param usuario
     * @return UsuarioDto
     */
    public static UsuarioDto convertUsuarioForDto(Usuario usuario){
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId(usuario.getId());
        usuarioDto.setNome(usuario.getNome());
        usuarioDto.setEmail(usuario.getEmail());
        usuarioDto.setDigitoUnicoDtoList(converterListDigitoUnicoForListDto(usuario.getDigitoUnicoList()));
        return usuarioDto;
    }

    /**
     * Converter um dto para usuário
     *
     * @param usuarioDto
     * @return Usuario
     */
    public static Usuario convertDtoForUsuario(UsuarioDto usuarioDto){
        Usuario usuario = new Usuario();
        usuario.setId(usuarioDto.getId());
        usuario.setNome(usuarioDto.getNome());
        usuario.setEmail(usuarioDto.getEmail());
        return usuario;
    }

    /**
     * Converter um dígito único em um dto.
     *
     * @param digitoUnico
     * @return DigitoUnicoDto
     */
    public static DigitoUnicoDto converterDigitoUnicoForDto(DigitoUnico digitoUnico){
        DigitoUnicoDto digitoUnicoDto = new DigitoUnicoDto();
        digitoUnicoDto.setId(digitoUnico.getId());
        digitoUnicoDto.setNumero(digitoUnico.getNumero());
        digitoUnicoDto.setMultiplicador(digitoUnico.getMultiplicador());
        digitoUnicoDto.setResultado(digitoUnico.getResultado());
        digitoUnicoDto.setIdUsuario(digitoUnico.getUsuario().getId());
        return digitoUnicoDto;
    }

    /**
     * Converter uma lista de digitos únicos em uma lista de dtos.
     *
     * @param digitoUnicoList
     * @return List<DigitoUnicoDto>
     */
    private static List<DigitoUnicoDto> converterListDigitoUnicoForListDto(List<DigitoUnico> digitoUnicoList){
        if(digitoUnicoList == null){
            return new ArrayList<>();
        }
        List<DigitoUnicoDto> digitoUnicoDtos = new ArrayList<>();
        for(DigitoUnico digitoUnico : digitoUnicoList){
            digitoUnicoDtos.add(converterDigitoUnicoForDto(digitoUnico));
        }

        return digitoUnicoDtos;
    }
}
