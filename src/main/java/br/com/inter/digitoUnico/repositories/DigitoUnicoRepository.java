package br.com.inter.digitoUnico.repositories;

import br.com.inter.digitoUnico.entities.DigitoUnico;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DigitoUnicoRepository extends JpaRepository<DigitoUnico, Long> {
}
